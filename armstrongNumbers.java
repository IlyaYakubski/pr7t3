import java.util.Scanner;

public class armstrongNumbers {

    public static boolean isArmstrongNumber(int number1) {
        int number2 = number1;
        int number3;
        int sum3 = 0;
        int pow = (number1 + "").length();
        while (number2 != 0) {
            number3 = number2 % 10;
            sum3 = (int) (sum3 + Math.pow(number3, pow));
            number2 = number2 / 10;
        }
        return number1 == sum3;

    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        for (int i = 1; i < a; i++) {
            System.out.println("Число Армстронга " + i + " " + isArmstrongNumber(i));
        }
    }

}


